package bandsville.bandsville;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ImageView profile_photo;
    private List<String> urls = new ArrayList<>();
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recommendation);
////        profile_photo=findViewById(R.id.user_profile_image);
//         urls.add("https://firebasestorage.googleapis.com/v0/b/meetup-9430f.appspot.com/o/usersdata%2FAtreyu%2015.jpg?alt=media&token=d7c7e52d-cea7-4102-a886-0392e6bc71c1");
//         urls.add("https://firebasestorage.googleapis.com/v0/b/meetup-9430f.appspot.com/o/usersdata%2FIMG_20171209_122857_077.jpg?alt=media&token=08e3922f-eb0f-431e-a3d6-6b68a7ae06cc");
//         urls.add("https://firebasestorage.googleapis.com/v0/b/meetup-9430f.appspot.com/o/usersdata%2Fkiller.jpg?alt=media&token=c840d4f5-de91-4eee-babf-218200b87626");
//         urls.add("https://firebasestorage.googleapis.com/v0/b/meetup-9430f.appspot.com/o/usersdata%2Fnine.jpg?alt=media&token=31deaee9-0aed-43c9-bcd5-7ceef6249b6b");
//        RequestOptions requestOptions=new RequestOptions();
//        requestOptions.centerCrop();
//        requestOptions.circleCrop();
//        requestOptions.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
//        Glide.with(getApplicationContext()).load(R.mipmap.rachit).apply(requestOptions).into(profile_photo);

    }


//
//    private class CustomAdapter extends PagerAdapter{
//
//        private LayoutInflater layoutInflater;
//
//
//
//        public CustomAdapter() {
//            this.layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        }
//
//        @Override
//        public int getCount() {
//            return urls.size();
//        }
//
//        @Override
//        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
//            return view == ((View)object);
//        }
//
//
//        @NonNull
//        @Override
//        public Object instantiateItem(@NonNull ViewGroup container, int position) {
//            View view = this.layoutInflater.inflate(R.layout.band_profile_fragment, container, false);
//            final ImageView user_profile_image=view.findViewById(R.id.user_profile_image);
//            Glide.with(getApplicationContext()).load(urls.get(position))
//                    .asBitmap()
//                    .format(DecodeFormat.PREFER_RGB_565)
//                    .centerCrop()
//                    .into(new BitmapImageViewTarget(user_profile_image) {
//                        @Override
//                        protected void setResource(Bitmap resource) {
//                            RoundedBitmapDrawable circularBitmapDrawable =
//                                    RoundedBitmapDrawableFactory.create(getApplicationContext().getResources(), resource);
//                            circularBitmapDrawable.setCircular(true);
//                            user_profile_image.setImageDrawable(circularBitmapDrawable);
//                        }
//                    });
//
//
//            container.addView(view);
//            return view;
//        }
//
//        @Override
//        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//            container.removeView((View) object);
//        }
//    }
//}


}