package bandsville.bandsville;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import bandsville.bandsville.Interface.ISharePreference;

public class RouterActivity extends AppCompatActivity {

    private EditText editText;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_router);
        button=findViewById(R.id.router_activity_send);
        editText=findViewById(R.id.router_activity_ip);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!editText.getText().toString().trim().equals("") ){

                    String localUrl="http://"+editText.getText().toString()+"/Rachit/bandsville-php/";//  http://<ip>/Rachit/bandsville-php/
                    localUrl=localUrl.trim();

                    new CustomSharedPref(getApplicationContext()).setSharedPref(ISharePreference.local_url,localUrl);
                    startActivity(new Intent(RouterActivity.this,LocationActivity.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Enter Local Ip first ",Toast.LENGTH_LONG).show();
                }



            }
        });

    }
}
