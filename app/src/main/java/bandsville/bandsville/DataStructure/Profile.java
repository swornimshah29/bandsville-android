package bandsville.bandsville.DataStructure;

public class Profile {
    private String pUrl;
    private String mStatus;
    private String mStatusAudio;
    private boolean sync;


    public String getpUrl() {
        return pUrl;
    }

    public void setpUrl(String pUrl) {
        this.pUrl = pUrl;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmStatusAudio() {
        return mStatusAudio;
    }

    public void setmStatusAudio(String mStatusAudio) {
        this.mStatusAudio = mStatusAudio;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }
}
