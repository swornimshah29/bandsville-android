package bandsville.bandsville.DataStructure;

public class Musicians {

   private String mId;
    private String mName;
    private String mType;
    private String mProfileP;
    private String mScore;
    private String bName;//associated band id
    private String mFollowers;
    private String distance;
    private String mLocationLat;
    private String mLocationLon;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }


    public String getmLocationLat() {
        return mLocationLat;
    }

    public void setmLocationLat(String mLocationLat) {
        this.mLocationLat = mLocationLat;
    }

    public String getmLocationLon() {
        return mLocationLon;
    }

    public void setmLocationLon(String mLocationLon) {
        this.mLocationLon = mLocationLon;
    }

    public String getmFollowers() {
        return mFollowers;
    }

    public void setmFollowers(String mFollowers) {
        this.mFollowers = mFollowers;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmProfileP() {
        return mProfileP;
    }

    public void setmProfileP(String mProfileP) {
        this.mProfileP = mProfileP;
    }

    public String getmScore() {
        return mScore;
    }

    public void setmScore(String mScore) {
        this.mScore = mScore;
    }

    public String getbName() {
        return bName;
    }

    public void setbName(String bName) {
        this.bName = bName;
    }
}
