package bandsville.bandsville.DataStructure;

public class Bands {
    private String bId;
    private String bName;
    private String bMembers;
    private String bType;
    private String bLogo;
    private String bFans;
    private String bRating;
    private String bLocationLat;
    private String bLocationLon;
    private String distance;//for comparison between bands



    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getbLocationLat() {
        return bLocationLat;
    }

    public void setbLocationLat(String bLocationLat) {
        this.bLocationLat = bLocationLat;
    }

    public String getbLocationLon() {
        return bLocationLon;
    }

    public void setbLocationLon(String bLocationLon) {
        this.bLocationLon = bLocationLon;
    }

    public String getbId() {
        return bId;
    }

    public void setbId(String bId) {
        this.bId = bId;
    }

    public String getbName() {
        return bName;
    }

    public void setbName(String bName) {
        this.bName = bName;
    }

    public String getbMembers() {
        return bMembers;
    }

    public void setbMembers(String bMembers) {
        this.bMembers = bMembers;
    }

    public String getbType() {
        return bType;
    }

    public void setbType(String bType) {
        this.bType = bType;
    }

    public String getbLogo() {
        return bLogo;
    }

    public void setbLogo(String bLogo) {
        this.bLogo = bLogo;
    }

    public String getbFans() {
        return bFans;
    }

    public void setbFans(String bFans) {
        this.bFans = bFans;
    }

    public String getbRating() {
        return bRating;
    }

    public void setbRating(String bRating) {
        this.bRating = bRating;
    }
}
