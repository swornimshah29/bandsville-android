package bandsville.bandsville.DataStructure;

public class Notification {

    private String req_to;//mid of sender
    private String req_from;//mid of receiver
    private String req_to_name;//mName
    private String req_from_name;//mName
    private String sync_to;//synced by receiver
    private String sync_from;//synced by sender
    private String nId;//id of row of that table
    private String bId;
    private String bName;

    public String getReq_to() {
        return req_to;
    }

    public void setReq_to(String req_to) {
        this.req_to = req_to;
    }

    public String getReq_from() {
        return req_from;
    }

    public void setReq_from(String req_from) {
        this.req_from = req_from;
    }

    public String getReq_to_name() {
        return req_to_name;
    }

    public void setReq_to_name(String req_to_name) {
        this.req_to_name = req_to_name;
    }

    public String getReq_from_name() {
        return req_from_name;
    }

    public void setReq_from_name(String req_from_name) {
        this.req_from_name = req_from_name;
    }

    public String getSync_to() {
        return sync_to;
    }

    public void setSync_to(String sync_to) {
        this.sync_to = sync_to;
    }

    public String getSync_from() {
        return sync_from;
    }

    public void setSync_from(String sync_from) {
        this.sync_from = sync_from;
    }

    public String getnId() {
        return nId;
    }

    public void setnId(String nId) {
        this.nId = nId;
    }

    public String getbId() {
        return bId;
    }

    public void setbId(String bId) {
        this.bId = bId;
    }

    public String getbName() {
        return bName;
    }

    public void setbName(String bName) {
        this.bName = bName;
    }
}
