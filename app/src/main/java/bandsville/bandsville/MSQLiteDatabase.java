package bandsville.bandsville;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandsville.bandsville.DataStructure.IType;
import bandsville.bandsville.DataStructure.Musicians;
import bandsville.bandsville.Interface.IDatabase;

/**
 * Created by Swornim on 1/27/2017.
 */
public class MSQLiteDatabase extends SQLiteOpenHelper implements IDatabase {

    private static Context CONTEXT;
    private Map<String, Object> contactListMap = new HashMap<>();


    public MSQLiteDatabase(Context context, Musicians musicians) {
        super(context, "freecall.db", null, 1);
        CONTEXT = context;

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE BANDS_RECOMMENDATION( ID INTEGER PRIMARY KEY AUTOINCREMENT,USERNUMBER TEXT,MUSICIANS_OBJECT TEXT); ");
        db.execSQL("CREATE TABLE PROFILE( ID INTEGER PRIMARY KEY AUTOINCREMENT,TYPE_ID TEXT,PROFILE_OBJECT TEXT); ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS FREE_USER;");
        onCreate(db);
    }


    public void insertUserContacts(String USERNAME, String USERNUMBER) {

        //TODO This method is only called one time during updates of the template messages
        ContentValues contentValues = new ContentValues();
        contentValues.put("USERNAME", USERNAME);
        contentValues.put("USERNUMBER", USERNUMBER);
        this.getWritableDatabase().insertOrThrow("FREE_USER", null, contentValues);//adds new rows
    }


    public Map<String, Object> readAllContacts() {

        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM FREE_USER", null);
        while (cursor.moveToNext()) {

            String id = cursor.getString(1);
            String number = cursor.getString(2);
            contactListMap.put(id, number);
        }
        cursor.close();
        return contactListMap;
    }

    @Override
    public void synchronize(Musicians musicians, String type) {

    }

    @Override
    public void insert(Musicians musicians,String type) {

        if(type.equals(IType.UPDATE_PROFILE_PICTURE)){
            if(already(IType.UPDATE_PROFILE_PICTURE_ID)){
                //dont insert do nothing
            }else{
                //insert new profile
                Gson gson=new Gson();
                ContentValues contentValues = new ContentValues();
                contentValues.put("TYPE_ID", type);
                this.getWritableDatabase().insertOrThrow("FREE_USER", null, contentValues);//adds new rows
            }
        }

    }

    @Override
    public boolean already(String typeId) {

        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM PROFILE WHERE TYPE_ID="+typeId, null);
        if(cursor.getCount()>0){
            /*rows exits for these type of update*/
            return true;
        }
        return false;
    }

//    public List<MinorDetails> readAllContactsFreecall()
//
//        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM FREE_USER", null);
//        while (cursor.moveToNext()) {
//
//        }
//        return contactList;
//    }
//
///*cod changed*/
//    @Override
//    public void insertMessage(MinorDetails messageObject) {
//        //for both update and insert
//
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("USERNUMBER", messageObject.getContactNumber());
//
//        if (messageObject.getIncomingMessageJ() != null) {
//            contentValues.put("INCOMING_MESSAGE", messageObject.getIncomingMessageJ());
//
//        }
//        if (messageObject.getIncomingMessageJ() != null) {
//            contentValues.put("OUTGOING_MESSAGE", messageObject.getOutgoingMessageJ());
//
//        }
//        if (userExists(messageObject.getContactNumber())) {
//            //update rows contentsss sds
//            if (userExists(messageObject.getContactNumber())) {
//                //update rows contentssss sds
//
//                Cursor readCursor = this.getReadableDatabase().rawQuery("SELECT * FROM FREE_USER WHERE USERNUMBER=" + messageObject.getContactNumber(), null);
//                readCursor.moveToFirst();
//
//
//                try {
//                    this.getWritableDatabase().update("FREE_USER", contentValues, "where id=" + readCursor.getString(0), null);
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                    //handle the exception
//                }
//                readCursor.close();
//
//
//            } else {
//                //create new row and update again
//                Cursor updateCursor = this.getReadableDatabase().rawQuery("SELECT * FROM FREE_USER WHERE USERNUMBER=" + messageObject.getContactNumber(), null);
//
//                this.getWritableDatabase().insert("FREE_USER", null, contentValues);//adds new rows
//                try {
//                    this.getWritableDatabase().rawQuery("UPDATE FREE_USER SET INCOMING_MESSAGE=" + messageObject.getIncomingMessageJ() + " WHERE ID=" + updateCursor.getColumnIndex("ID"), null);
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                    //handle the exception
//                }
//                updateCursor.close();
//
//            }
//
//        }
//
//    }
//
//    @Override
//    public MinorDetails getMessage(String phoneNumber, int index) {
//        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM FREE_USER WHERE USERNUMBER= " + phoneNumber, null);
//        cursor.moveToFirst();
//        Log.i("tags", "sqlite" + cursor.getString(2));
//        String incomingMessageJ = cursor.getString(2);
//
//        //convert to object
//        Gson gson = new Gson();
//        MinorDetails eachMessage = gson.fromJson(incomingMessageJ, MinorDetails.class).getMessageList().get(index);
//        return eachMessage;
//    }
//
//    @Override
//    public boolean userExists(String phoneNumber) {
//        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM FREE_USER", null);
//        while (cursor.moveToNext()) {
//            if (cursor.getString(1).equals(phoneNumber)) {
//                Log.i("mytag", "true");
//                cursor.close();
//                return true;
//            }
//        }
//        Log.i("mytag", "false");
//        cursor.close();
//        return false;
//    }
//
//
//    public void insertIntoCache(MinorDetails messageObject) {
//        int userCounter = 0;
//        int messageConter = 0;
//        boolean messageFound = false;
//        boolean userFound = false;
//        int userAt = -1;//assume not found to avoid zero by default
//        int messageAt = -1;//assume not found to avoid zero by default
//
//        ContentValues contentValues = new ContentValues();
//        if (userExists(messageObject.getContactNumber())) {
//
//            Cursor readCursor = this.getReadableDatabase().rawQuery("SELECT * FROM FREE_USER WHERE USERNUMBER="+messageObject.getContactNumber(), null);
//            readCursor.moveToFirst();
//            Cache cacheObject = new Gson().fromJson(readCursor.getString(2), Cache.class);
//
//            for (Map<String, List<Map<String, MinorDetails>>> eachUser : cacheObject.getFriends()) {
//
//                if (eachUser.containsKey(messageObject.getContactNumber())) {
//                    userAt = userCounter;
//                    userFound=true;
//                    Log.i("mytags", "found user at  " + userAt);
//
//                    //find which messages index
//                    for (Map<String, MinorDetails> eachMessage : eachUser.get(messageObject.getContactNumber())){
//
//                        for (Map.Entry<String, MinorDetails> eachObject : eachMessage.entrySet()){
//
//                            if(eachObject.getKey().equals("message" + messageObject.getIndex())){
//                                messageAt = messageConter;
//                                messageFound = true;
//                                Log.i("mytags", "found message at  " + messageAt);
//
//                                break;
//                            }
//                            ++messageConter;
//                        }
//                        if (messageFound) {
//                            break;
//                        }
//                    }
//                    break;
//                }
//                ++userCounter;
//            }
//
//
//            if(userFound) {
//                if (messageFound) {
//                    /*Update the existing messageMap*/
//                    cacheObject
//                            .getFriends()
//                            .get(userAt)
//                            .get(messageObject.getContactNumber())
//                            .get(messageAt)
//                            .put("message" + messageObject.getIndex(), messageObject);
//                } else {
//                    /*create new messageMap*/
//                    Map<String, MinorDetails> newMessage = new HashMap<>();
//                    newMessage.put("message" + messageObject.getIndex(), messageObject);
//                    cacheObject
//                            .getFriends()
//                            .get(userAt)
//                            .get(messageObject.getContactNumber())
//                            .add(newMessage);
//
//                }
//
//                contentValues.put("INCOMING_MESSAGE", new Gson().toJson(cacheObject));
//                this.getWritableDatabase().update("FREE_USER", contentValues, "ID=" + readCursor.getString(0), null);
//                Cursor readCursor1 = this.getReadableDatabase().rawQuery("SELECT * FROM FREE_USER WHERE USERNUMBER=" + messageObject.getContactNumber(), null);
//                readCursor1.moveToFirst();
//                Log.i("mytags", "old user " + readCursor1.getString(2));
//                readCursor1.close();
//
//            }
//
////            contentValues.put("USERNUMBER",messageObject.getContactNumber());
//
//
////        this.getWritableDatabase().insert("FREE_USER",null,contentValues);//adds new rows
//
//            //}
//        }
//        else{
//            /*It creates one message object if row dont exist so there is no chance for null point exception*/
//            Cache cacheObject=new Cache();
//            List<Map<String,List<Map<String,MinorDetails>>>> friends=new ArrayList<>();
//            Map<String,List<Map<String,MinorDetails>>> eachUserMap=new HashMap<>();
//            List<Map<String,MinorDetails>> eachMessage=new ArrayList<>();
//            Map<String,MinorDetails> object=new HashMap<>();
//
//
//            object.put("message"+messageObject.getIndex(),messageObject);
//            eachMessage.add(object);
//            eachUserMap.put(messageObject.getContactNumber(),eachMessage);
//            friends.add(eachUserMap);
//
//            cacheObject.setFriends(friends);
//            contentValues.put("USERNUMBER",messageObject.getContactNumber());
//            contentValues.put("INCOMING_MESSAGE",new Gson().toJson(cacheObject));
//
//            this.getWritableDatabase().insert("FREE_USER",null,contentValues);//adds new rows
//            Cursor readCursor=this.getReadableDatabase().rawQuery("SELECT * FROM FREE_USER WHERE USERNUMBER="+messageObject.getContactNumber(),null);
//            readCursor.moveToFirst();
//            Log.i("mytags","New User "+readCursor.getString(2));
//            readCursor.close();
//
//        }
//
//    }
//    /*code*/
//
//    /*chanf*/

}




