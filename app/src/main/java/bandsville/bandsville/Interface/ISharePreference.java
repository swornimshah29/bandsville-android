package bandsville.bandsville.Interface;

public interface ISharePreference {

    public String registrationSuccess="registrationSuccess";
    public String uPNumber="uPNumber";
    public String uName="uName";
    public String bId="bId";
    public String bName="bName";
    public String profile_picture_path="profile_picture_path";
    public String server_public_url="https://bandsville.000webhostapp.com/";
    public String local_url="local_url";

}
