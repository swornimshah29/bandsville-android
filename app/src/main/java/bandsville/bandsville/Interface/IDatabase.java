package bandsville.bandsville.Interface;

import bandsville.bandsville.DataStructure.Musicians;

public interface IDatabase {
     void synchronize(Musicians musicians,String type);//update if the server syncs
     void insert(Musicians  musicians,String type);
     boolean already(String typeId);
}
