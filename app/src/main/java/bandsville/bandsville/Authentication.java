package bandsville.bandsville;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import bandsville.bandsville.Fragments.CreateBandDetails;
import bandsville.bandsville.Fragments.LoginFragment;
import bandsville.bandsville.Fragments.MusicianTypeFragment;
import bandsville.bandsville.Fragments.RegistrationFragment;
import bandsville.bandsville.Interface.ISharePreference;

public class Authentication extends AppCompatActivity {
    /*
    for login and registration
     */


    private Button login_register_button;
    private ViewPageAdapter viewPageAdapter;
    public ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authentication);
        viewPager= findViewById(R.id.authentiation_vp);
        viewPageAdapter=new ViewPageAdapter(getSupportFragmentManager());

        viewPageAdapter.addFragments(new RegistrationFragment());
        viewPageAdapter.addFragments(new LoginFragment());
        viewPageAdapter.addFragments(new MusicianTypeFragment());
        viewPager.setAdapter(viewPageAdapter);

        if(new  CustomSharedPref(getApplicationContext()).getSharedPref(ISharePreference.registrationSuccess).equals("success")){
            Intent intent=new Intent(Authentication.this,HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }else{
            viewPager.setCurrentItem(0);
        }


    }

    public ViewPager getViewPager() {
        if (null == viewPager) {
            viewPager = findViewById(R.id.authentiation_vp);
        }
        return viewPager;
    }


    public class ViewPageAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> fragments=new ArrayList<>();

        public void addFragments( Fragment fragments){
            this.fragments.add(fragments);
        }


        public ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {

            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0:
                    return "Login";
                case 1:
                    return "Registration";

            }
            return null;

        }


    }
}
