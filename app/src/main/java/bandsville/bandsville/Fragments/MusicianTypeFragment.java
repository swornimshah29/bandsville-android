package bandsville.bandsville.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bandsville.bandsville.Authentication;
import bandsville.bandsville.CreateBand;
import bandsville.bandsville.R;

public class MusicianTypeFragment extends Fragment{

    private List<String> bandTypeList=new ArrayList<>();
    private GridView createBandType_gv;
    private ArrayAdapter<String> adapter;
    public static String mType=null;

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mView=inflater.inflate(R.layout.createbandtype,container,false);
        createBandType_gv=mView.findViewById(R.id.createBandType_gv);
        adapter=new BandTypeAdapter(getContext(),bandTypeList);
        createBandType_gv.setAdapter(adapter);
        intializeMusicianTypes();



        return mView;
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private class BandTypeAdapter extends ArrayAdapter<String>{

        public BandTypeAdapter(@NonNull Context context, List<String> bandTypeList) {
            super(context, R.layout.createbandtype_custom_layout,bandTypeList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View mView=convertView;
            if(convertView==null) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                mView = inflater.inflate(R.layout.createbandtype_custom_layout, parent, false);
            }

            TextView createBandType_custom_tv=mView.findViewById(R.id.createBandType_custom_tv);
            createBandType_custom_tv.setTag(position);
            createBandType_custom_tv.setText(bandTypeList.get(position));
            createBandType_custom_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mType=bandTypeList.get((int)v.getTag());
                    ((Authentication)getActivity()).getViewPager().setCurrentItem(0);

                }
            });



            return mView;
        }
    }





    private void intializeMusicianTypes(){
        if(bandTypeList!=null){
            if(bandTypeList.size()>0){
                bandTypeList.clear();
                bandTypeList.add("Drumer");
                bandTypeList.add("Panist");
                bandTypeList.add("Lead Guitarist");
                bandTypeList.add("Keywordist");
                bandTypeList.add("Vocalist");
                bandTypeList.add("Base Guitarist");
                bandTypeList.add("Fluteist");
                bandTypeList.add("Heavy metal");
                bandTypeList.add("Violanist");
                bandTypeList.add("Trumtist");
                bandTypeList.add("Madalist");
                bandTypeList.add("Saxophoneist");
                adapter=new BandTypeAdapter(getContext(),bandTypeList);
                createBandType_gv.setAdapter(adapter);

            }else{
                bandTypeList.add("Drummer");
                bandTypeList.add("Panist");
                bandTypeList.add("Lead Guitarist");
                bandTypeList.add("Keywordist");
                bandTypeList.add("Vocalist");
                bandTypeList.add("Base Guitarist");
                bandTypeList.add("Fluteist");
                bandTypeList.add("Violanist");
                bandTypeList.add("Trumpetist");
                bandTypeList.add("Madalist");
                bandTypeList.add("Saxophoneist");
                adapter=new BandTypeAdapter(getContext(),bandTypeList);
                createBandType_gv.setAdapter(adapter);
            }
        }


    }

}
