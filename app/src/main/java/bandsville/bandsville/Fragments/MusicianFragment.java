package bandsville.bandsville.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandsville.bandsville.CustomSharedPref;
import bandsville.bandsville.DataStructure.Bands;
import bandsville.bandsville.DataStructure.Musicians;
import bandsville.bandsville.DataStructure.Notification;
import bandsville.bandsville.DataStructure.Status;
import bandsville.bandsville.Interface.ISharePreference;
import bandsville.bandsville.R;

public class MusicianFragment extends Fragment {

    private List<Musicians> musicianRecommendation=new ArrayList<>();
    private GridView gridView;
    private ArrayAdapter<Musicians> adapter;
    private int maxTry=0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getMusicianRecommendation();
        View mView=inflater.inflate(R.layout.musician_fragment,container,false);
        gridView=mView.findViewById(R.id.musician_fragment_gv);
        adapter=new RecommendationAdapter(getContext(),musicianRecommendation);
        gridView.setAdapter(adapter);

        return mView;
    }





    private void getMusicianRecommendation(){

        if(getContext()==null)
            return;

        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://192.168.1.111/Rachit/bandsville-php/Musicians.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Musicians.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag", "response Musicians "+response);
                        if(response!=null || !response.equals("")){
                            Type listType = new TypeToken<List<Musicians>>() {}.getType();
                            musicianRecommendation=new Gson().fromJson(response,listType);
                            maxTry=0;//reset
                            adapter=new RecommendationAdapter(getContext(),musicianRecommendation);
                            gridView.setAdapter(adapter);
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if(maxTry==4){
                    Log.i("mytag", "try again connection timeout ");

                }else{
                    maxTry++;
                    getMusicianRecommendation();

                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Musicians musicians=new Musicians();
                musicians.setmLocationLat("27");
                musicians.setmLocationLon("85.5");
                musicians.setmId(new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uPNumber));
                Map<String,String> map=new HashMap<>();
                map.put("getRecommendedMusicians",new Gson().toJson(musicians));
                map.put("filterType","location");//default location

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }



    private class RecommendationAdapter extends ArrayAdapter<Musicians> {

        public RecommendationAdapter(@NonNull Context context, List<Musicians> bandsRecommendation) {
            super(context, R.layout.band_fragment,musicianRecommendation);
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View mView=convertView;

            if(convertView==null){
                LayoutInflater inflater=LayoutInflater.from(getContext());
                mView=inflater.inflate(R.layout.musician_fragment_custom,parent,false);
            }

            ImageView musician_fragment_custom_iv=mView.findViewById(R.id.musician_fragment_custom_iv);
            TextView musician_fragment_custom_bName_mtype=mView.findViewById(R.id.musician_fragment_custom_bName_mtype);
            TextView musician_fragment_custom_mName=mView.findViewById(R.id.musician_fragment_custom_mName);
            ImageView musician_fragment_custom_invite=mView.findViewById(R.id.musician_fragment_custom_invite);
            ImageView musician_fragment_custom_follow=mView.findViewById(R.id.musician_fragment_custom_follow);
            ImageView musician_fragment_custom_rating=mView.findViewById(R.id.musician_fragment_custom_rating);


            musician_fragment_custom_mName.setText(musicianRecommendation.get(position).getmName());
            musician_fragment_custom_bName_mtype.setText(musicianRecommendation.get(position).getmName()+"# "+musicianRecommendation.get(position).getbName());
            musician_fragment_custom_iv.setTag(R.id.musician_fragment_custom_iv,position);
            musician_fragment_custom_invite.setTag(position);
            //key is unique for the widget and value is position
            musician_fragment_custom_invite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (new CustomSharedPref(getContext()).getSharedPref(ISharePreference.bName).equals("none")) {
                        Toast.makeText(getContext(), "You are not associated to any bands", Toast.LENGTH_LONG).show();
                    } else {

                        Notification nObject = new Notification();
                        nObject.setReq_from_name(new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uName));
                        nObject.setReq_from(new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uPNumber));
                        nObject.setReq_to(musicianRecommendation.get((int)v.getTag()).getmId());
                        nObject.setReq_to_name(musicianRecommendation.get((int)v.getTag()).getmName());
                        nObject.setSync_from("null");
                        nObject.setSync_to("null");
                        nObject.setbId(new CustomSharedPref(getContext()).getSharedPref(ISharePreference.bId));
                        nObject.setbName(new CustomSharedPref(getContext()).getSharedPref(ISharePreference.bName));

                        sendRequestToJoinBand(nObject);
                    }
                }
            });

            musician_fragment_custom_follow.setTag(position);
            musician_fragment_custom_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //send follow request
                    String followedBy=new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uPNumber);
                    String followedTo=musicianRecommendation.get(position).getmId();
                    followMusicians(followedBy,followedTo);

                }
            });

            musician_fragment_custom_rating.setTag(position);

            musician_fragment_custom_rating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //send rated value
                    ratingDialogBox((int)v.getTag());
                }
            });



            try {
                Glide.with(getContext()).load(URLDecoder.decode(musicianRecommendation.get(position).getmProfileP(),"UTF-8"))
                        .asBitmap()
                        .placeholder(R.mipmap.default_image)
                        .format(DecodeFormat.PREFER_RGB_565)//high quality image
                        .into(musician_fragment_custom_iv);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            return mView;
        }
    }




    private void sendRatingValue(final String mId, final String value, final String ratedBy){
        //mId will be the unique id of the musicians


        if(getContext()==null)
            return;

        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://192.168.1.111/Rachit/bandsville-php/Musicians.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Musicians.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag","rating response "+response);
                        Status status=new Gson().fromJson(response,Status.class);
                        if(status.getStatusCode().equals("success")){
                            Toast.makeText(getContext(),"Successfully rated",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(getContext(),"you may have already rated",Toast.LENGTH_LONG).show();

                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if(maxTry==4){
                    Log.i("mytag", "try again connection timeout ");

                }else{
                    maxTry++;
                    sendRatingValue(mId,value,ratedBy);
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("rateMusicians","true");
                map.put("ratedBy",ratedBy);
                map.put("ratedScore",value);
                map.put("mId",mId);

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


private void followMusicians(final String followedBy, final String followedTo){
        //mId will be the unique id of the musicians


        if(getContext()==null)
            return;

        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://192.168.1.111/Rachit/bandsville-php/Musicians.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Musicians.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag","rating response "+response);
                        Status status=new Gson().fromJson(response,Status.class);
                        if(status.getStatusCode().equals("success")){
                            Toast.makeText(getContext(),"Successfully Followed",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(getContext(),"you may have already Followed",Toast.LENGTH_LONG).show();

                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if(maxTry==4){
                    Log.i("mytag", "try again connection timeout ");

                }else{
                    maxTry++;
                    followMusicians(followedBy,followedTo);
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("followMusicians","true");
                map.put("followedBy",followedBy);
                map.put("followedTo",followedTo);

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }




    private void sendRequestToJoinBand(final Notification nObject) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                "http://192.168.1.111/Rachit/bandsville-php/Notification.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Notification.php",
//                Value.SERVER_ADDRESS+"Payment.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag", "sendRequestResponse " + response);
                        Status status = new Gson().fromJson(response, Status.class);
                        maxTry = 0;

                        if (status.getStatusCode().equals("success")) {
                            Toast.makeText(getContext(), "Successfully sent the request", Toast.LENGTH_SHORT).show();
                        } else if(status.getStatusCode().equals("already")){
                            Toast.makeText(getContext(), "You have already sent request", Toast.LENGTH_SHORT).show();
                        } else{
                            Toast.makeText(getContext(), "Your Request failed to sent", Toast.LENGTH_SHORT).show();

                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if (maxTry == 4) {
                    Log.i("mytag", "try again connection timeout ");

                } else {
                    maxTry++;
                    sendRequestToJoinBand(nObject);

                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<>();
                map.put("requestToJoinBand", "yes");
                map.put("notificationObject",new Gson().toJson(nObject) );

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }


    private void ratingDialogBox(final int position){

        Dialog dialog=new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        LayoutInflater inflater=LayoutInflater.from(getContext());
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.rating_custom_design);
        RatingBar rating_custom_design_rb=dialog.findViewById(R.id.rating_custom_design_rb);
        final TextView rating_custom_design_tv=dialog.findViewById(R.id.rating_custom_design_tv);
        rating_custom_design_rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                //send the rating value as rating
                rating_custom_design_tv.setText(String.valueOf(rating));
                sendRatingValue(musicianRecommendation.get(position).getmId(),String.valueOf(rating),new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uName));
            }
        });

        dialog.show();

    }

}
