package bandsville.bandsville.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandsville.bandsville.CustomSharedPref;
import bandsville.bandsville.DataStructure.Bands;
import bandsville.bandsville.DataStructure.Status;
import bandsville.bandsville.Interface.ISharePreference;
import bandsville.bandsville.R;

public class BandFragment extends Fragment {
    private List<Bands> bandsRecommendation=new ArrayList<>();
    private ListView listView;
    private ArrayAdapter<Bands> adapter;
    private int maxTry=0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(new CustomSharedPref(getContext()).getSharedPref("bId").equals("none")){
            Toast.makeText(getContext(),"No any band associated",Toast.LENGTH_LONG).show();
        }else{
            getBandsRecommendation();

        }
        View mView=inflater.inflate(R.layout.band_fragment,container,false);
        listView=mView.findViewById(R.id.band_fragment_lv);

        return mView;
    }





    private void getBandsRecommendation(){

        if(getContext()==null)
            return;

        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://192.168.1.111/Rachit/bandsville-php/Bands.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Bands.php",
//                Value.SERVER_ADDRESS+"Payment.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response!=null){
                            Log.i("mytag", "response bands "+response);
                            Type listType = new TypeToken<List<Bands>>() {}.getType();
                            bandsRecommendation=new Gson().fromJson(response,listType);
                            maxTry=0;//reset
                            adapter=new RecommendationAdapter(getContext(),bandsRecommendation);
                            listView.setAdapter(adapter);
                        }
                    }

                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if(maxTry==4){
                    Log.i("mytag", "try again connection timeout ");

                }else{
                    maxTry++;
                    getBandsRecommendation();

                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("getRecommendedBands","true");
                map.put("uPNumber",new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uPNumber));

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }



    private class RecommendationAdapter extends ArrayAdapter<Bands> {

        public RecommendationAdapter(@NonNull Context context,List<Bands> bandsRecommendation) {
            super(context, R.layout.band_fragment,bandsRecommendation);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View mView=convertView;

            if(convertView==null){
                LayoutInflater inflater=LayoutInflater.from(getContext());
                mView=inflater.inflate(R.layout.band_fragment_custom,parent,false);
            }
            final ImageView bLogo=mView.findViewById(R.id.band_fragment_custom_bLogo);
            TextView bName=mView.findViewById(R.id.band_fragment_custom_bName);
            TextView bType=mView.findViewById(R.id.band_fragment_custom_bType);
            TextView bfans=mView.findViewById(R.id.band_fragment_custom_bFans);
            RatingBar band__fragment_custom_rating=mView.findViewById(R.id.band__fragment_custom_rating);
            final Button band_fragment_custom_bFans_btn=mView.findViewById(R.id.band_fragment_custom_bFans_btn);
            band_fragment_custom_bFans_btn.setTag(position);

            band_fragment_custom_bFans_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String followedBy=new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uPNumber);
                    String followedTo=bandsRecommendation.get((int)v.getTag()).getbId();
                    followBands(followedBy,followedTo);
                }
            });

            band__fragment_custom_rating.setTag(position);
            band__fragment_custom_rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    sendRatingValue(bandsRecommendation.get((int)ratingBar.getTag()).getbId(),String.valueOf(rating),new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uName));
                }
            });

            try {
                Glide.with(getContext()).load(URLDecoder.decode(bandsRecommendation.get(position).getbLogo(),"UTF-8"))
                        .asBitmap()
                        .format(DecodeFormat.PREFER_RGB_565)//high quality image
                        .centerCrop()
                        .into(bLogo);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            bName.setText(bandsRecommendation.get(position).getbName());
            bType.setText(bandsRecommendation.get(position).getbType());
            bfans.setText(bandsRecommendation.get(position).getbFans());



            return mView;
        }
    }

    private void sendRatingValue(final String bId, final String value, final String ratedBy){
        //mId will be the unique id of the musicians


        if(getContext()==null)
            return;

        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://192.168.1.111/Rachit/bandsville-php/Bands.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Bands.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag","rating response "+response);
                        Status status=new Gson().fromJson(response,Status.class);
                        if(status.getStatusCode().equals("success")){
                            Toast.makeText(getContext(),"Successfully rated",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(getContext(),"you may have already rated",Toast.LENGTH_LONG).show();

                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if(maxTry==4){
                    Log.i("mytag", "try again connection timeout ");

                }else{
                    maxTry++;
                    sendRatingValue(bId,value,ratedBy);
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("rateBands","true");
                map.put("ratedBy",ratedBy);
                map.put("ratedScore",value);
                map.put("bId",bId);

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private void followBands(final String followedBy, final String followedTo){
        //mId will be the unique id of the musicians


        if(getContext()==null)
            return;

        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://192.168.1.111/Rachit/bandsville-php/Bands.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Bands.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag","followBands response "+response);
                        Status status=new Gson().fromJson(response,Status.class);
                        if(status.getStatusCode().equals("success")){
                            Toast.makeText(getContext(),"Successfully Followed",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(getContext(),"you may have already Followed",Toast.LENGTH_LONG).show();

                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if(maxTry==4){
                    Log.i("mytag", "try again connection timeout ");

                }else{
                    maxTry++;
                    followBands(followedBy,followedTo);
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("followBands","true");
                map.put("followedBy",followedBy);
                map.put("followedTo",followedTo);

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

}



