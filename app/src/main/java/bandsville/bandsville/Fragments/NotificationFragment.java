package bandsville.bandsville.Fragments;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandsville.bandsville.CustomSharedPref;
import bandsville.bandsville.DataStructure.Notification;
import bandsville.bandsville.DataStructure.Status;
import bandsville.bandsville.Interface.ISharePreference;
import bandsville.bandsville.R;

public class NotificationFragment extends Fragment {

    private int maxTry = 0;

    private List<Notification> notificationList = new ArrayList<>();
    private ListView listView;
    private ArrayAdapter<Notification> adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getNotification();

        View mView = inflater.inflate(R.layout.notification_fragment, container, false);
        listView = mView.findViewById(R.id.notification_fragment_lv);
        adapter = new NotificationAdapter(getContext(), notificationList);
        listView.setAdapter(adapter);
        return mView;
    }


    private class NotificationAdapter extends ArrayAdapter<Notification> {

        public NotificationAdapter(@NonNull Context context, List<Notification> notificationList) {
            super(context, R.layout.notification_fragment_custom_request, notificationList);
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            if(notificationList.
                    get(position).
                    getReq_to().
                    equals(new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uPNumber))){
                return 0;//request type
            }
            return 1;//response type
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View mView = convertView;

            int type=getItemViewType(position);
            Notification nObject = new Notification();
            nObject = notificationList.get(position);

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(getContext());

                if(type==0){
                    mView = inflater.inflate(R.layout.notification_fragment_custom_request, parent, false);
                }else{
                    mView = inflater.inflate(R.layout.notification_fragment_custom_response, parent, false);

                }
            }


            if(type==0){
                TextView notification_fragment_custom_request_tv = mView.findViewById(R.id.notification_fragment_custom_request_tv);
                Button notification_fragment_custom_request_accept_iv = mView.findViewById(R.id.notification_fragment_custom_request_accept_iv);
                Button notification_fragment_custom_request_reject_iv = mView.findViewById(R.id.notification_fragment_custom_request_reject_iv);

                notification_fragment_custom_request_accept_iv.setTag(position);
                notification_fragment_custom_request_reject_iv.setTag(position);


                notification_fragment_custom_request_accept_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        syncNotificationFromReceiver(notificationList.get((int) v.getTag()).getnId(), "true");
                        notificationList.remove((int) v.getTag());
                        adapter.notifyDataSetChanged();

                    }
                });

                notification_fragment_custom_request_reject_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        syncNotificationFromReceiver(notificationList.get((int) v.getTag()).getnId(), "false");
                        notificationList.remove((int) v.getTag());
                        adapter.notifyDataSetChanged();

                    }
                });

                notification_fragment_custom_request_tv.setText(nObject.getReq_from_name() + " Invited You To Join " + nObject.getbName());


            }else{

                TextView notification_fragment_custom_response_tv=mView.findViewById(R.id.notification_fragment_custom_response_tv);
                notification_fragment_custom_response_tv.setTag(position);
                if(nObject.getSync_to().equals("true")){
                    notification_fragment_custom_response_tv.setText(nObject.getReq_to_name()+" Accepted Your Invite");

                }else{
                    notification_fragment_custom_response_tv.setText(nObject.getReq_to_name()+" Said Sorry cannot join right now");

                }

                notification_fragment_custom_response_tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        syncNotificationFromSender(notificationList.get((int) v.getTag()).getnId());
                        notificationList.remove((int) v.getTag());
                        adapter.notifyDataSetChanged();
                    }
                });

            }



            return mView;
        }

    }


    private void syncNotificationFromSender(final String nId) {

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                "http://192.168.1.111/Rachit/bandsville-php/Notification.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Notification.php",
//                Value.SERVER_ADDRESS+"Payment.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag", "sendRequestResponse " + response);
                        Status status = new Gson().fromJson(response, Status.class);
                        maxTry = 0;

                        if (status.getStatusCode().equals("success")) {
                            Toast.makeText(getContext(), "Successfully sent", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), "Failed to sent ", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if (maxTry == 4) {
                    Log.i("mytag", "try again connection timeout ");

                } else {
                    maxTry++;
                    syncNotificationFromSender(nId);

                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<>();
                map.put("syncNotificationFromSender", "yes");
                map.put("nId", nId);

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void syncNotificationFromReceiver(final String nId, final String status) {

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                "http://192.168.1.111/Rachit/bandsville-php/Notification.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Notification.php",
//                Value.SERVER_ADDRESS+"Payment.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag", "sendRequestResponse " + response);
                        Status status = new Gson().fromJson(response, Status.class);
                        maxTry = 0;

                        if (status.getStatusCode().equals("success")) {
                            Toast.makeText(getContext(), "Successfully sent", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), "Failed to sent ", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if (maxTry == 4) {
                    Log.i("mytag", "try again connection timeout ");

                } else {
                    maxTry++;
                    syncNotificationFromReceiver(nId, status);

                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<>();
                map.put("syncNotificationFromReceiver", "yes");
                map.put("nId", nId);
                map.put("status", status);

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getNotification() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                        "http://192.168.1.111/Rachit/bandsville-php/Notification.php",
                        new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Notification.php",
                        new com.android.volley.Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.i("mytag", "getNotification " + response);
                                Type listType = new TypeToken<List<Notification>>() {}.getType();
                                notificationList = new Gson().fromJson(response, listType);

                                //empty previous messages

                                if (notificationList.size() > 0) {

                                    MediaPlayer mPlayer = MediaPlayer.create(getContext(), R.raw.definite);
                                    mPlayer.start();
                                    //make a sound notification and update listview
                                    adapter = new NotificationAdapter(getContext(), notificationList);
                                    listView.setAdapter(adapter);
                                }
                                maxTry=0;//reset
                            }
                        }, new com.android.volley.Response.ErrorListener() {


                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (maxTry == 4) {
                            Log.i("mytag", "try again connection timeout ");

                        } else {
                            maxTry++;
                            getNotification();

                        }

                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> map = new HashMap<>();
                        map.put("getNotification", "yes");
                        map.put("uPNumber", new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uPNumber));
                        Log.i("mytag", "getNotification request" + new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uPNumber));


                        return map;
                    }


                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        8000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);

            }

        }).start();

    }

}
