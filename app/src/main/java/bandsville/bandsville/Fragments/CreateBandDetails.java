package bandsville.bandsville.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandsville.bandsville.CreateBand;
import bandsville.bandsville.CustomSharedPref;
import bandsville.bandsville.DataStructure.Bands;
import bandsville.bandsville.DataStructure.Status;
import bandsville.bandsville.HomeActivity;
import bandsville.bandsville.Interface.ISharePreference;
import bandsville.bandsville.MainActivity;
import bandsville.bandsville.R;

import static android.app.Activity.RESULT_OK;

public class CreateBandDetails extends Fragment {
    private ImageView createbanddetails_logo;
    private EditText createbanddetails_name;
    private ImageView createbanddetails_back_buton;
    private FloatingActionButton createbanddetails_send;
    private Bands newBandObject = new Bands();
    private List<String> pathList;
    //simple location access
    private FusedLocationProviderClient fusedLocationProviderClient;
    private ProgressBar createbanddetails_progressbar;
    private TextView createbanddetails_progress_tv;
    private static Location location = null;
    private int maxTry = 0;
    LocationListener locationListener;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.createbanddetails_custom_layout, container, false);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        createbanddetails_logo = mView.findViewById(R.id.createbanddetails_logo);
        createbanddetails_name = mView.findViewById(R.id.createbanddetails_name);
        createbanddetails_back_buton = mView.findViewById(R.id.createbanddetails_back_buton);
        createbanddetails_send = mView.findViewById(R.id.createbanddetails_send);
        createbanddetails_progressbar = mView.findViewById(R.id.createbanddetails_progressbar);
        createbanddetails_progress_tv = mView.findViewById(R.id.createbanddetails_progress_tv);

        createbanddetails_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), PickImageActivity.class);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 1);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
            }
        });


        createbanddetails_back_buton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CreateBand) getActivity()).getViewPager().setCurrentItem(0);
            }
        });

        createbanddetails_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeNewBand();
            }
        });


        return mView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {
            pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                StringBuilder sb = new StringBuilder("");
                for (int i = 0; i < pathList.size(); i++) {
                    Log.i("mytag","path url " + pathList.get(i));
                    Log.i("mytag", "file name " + new File(pathList.get(i)).getName());
                }

                Glide.with(getContext()).load(pathList.get(0))
                        .asBitmap()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .centerCrop()
                        .into(new BitmapImageViewTarget(createbanddetails_logo) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                createbanddetails_logo.setImageDrawable(circularBitmapDrawable);
                            }
                        });

                new CustomSharedPref(getContext()).setSharedPref("band_profile_image",pathList.get(0));//default none if value is null

            }

        }
    }

    private void initializeData(){
        String locationLat=new CustomSharedPref(getContext()).getSharedPref("USER_CURRENT_LOCATION_LAT");
        String locationLon=new CustomSharedPref(getContext()).getSharedPref("USER_CURRENT_LOCATION_LON");
        if(locationLat.equals("null") && locationLon.equals("null")){
            Toast.makeText(getContext(),"Location is null", Toast.LENGTH_LONG).show();
        }else{
            newBandObject.setbLocationLat(locationLat);
            newBandObject.setbLocationLon(locationLon);
            newBandObject.setbName(createbanddetails_name.getText().toString());
            newBandObject.setbType(CreateBandType.bType);
            sendNewBandData();
        }


    }


    private void makeNewBand(){

        InputStream stream = null;
        String photoUrl= new CustomSharedPref(getContext()).getSharedPref("band_profile_image");
        File uploadingFile=new File(photoUrl);
        StorageReference storageReference=FirebaseStorage.getInstance().getReference().child("band-profile").child(uploadingFile.getName());

        try {

            stream = new FileInputStream(uploadingFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            createbanddetails_progressbar.setVisibility(View.VISIBLE);

            UploadTask uploadTask = storageReference.putStream(stream);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    createbanddetails_progressbar.setVisibility(View.INVISIBLE);
                    //set bLogo url
                    newBandObject.setbLogo(taskSnapshot.getDownloadUrl().toString());
                    initializeData();



                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    //progress report

                }


            });


    }


    private void sendNewBandData(){

        //if(isset($_POST['makeNewBand']) && isset($_POST['uPNumber'])){

            RequestQueue requestQueue= Volley.newRequestQueue(getContext());
            StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                    "http://192.168.1.111/Rachit/bandsville-php/Bands.php",
                    new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Bands.php",
//                Value.SERVER_ADDRESS+"Payment.php",
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("mytag", "registration response "+response);
                            Status status=new Gson().fromJson(response,Status.class);
                            if(status.getStatusCode().equals("success")){
                                Toast.makeText(getContext(), "Successfully created your new band", Toast.LENGTH_SHORT).show();
                                new CustomSharedPref(getContext()).setSharedPref(ISharePreference.bId,status.getStatusType());//status type holds bId(lazy to do in another way for now)
                                new CustomSharedPref(getContext()).setSharedPref(ISharePreference.bName,newBandObject.getbName());
                                location=null;
                                startActivity(new Intent(getActivity(), HomeActivity.class));

                                maxTry=0;
                            }else{
                                Toast.makeText(getContext(), "Error in the Network", Toast.LENGTH_SHORT).show();

                            }

                        }
                    }, new com.android.volley.Response.ErrorListener() {


                @Override
                public void onErrorResponse(VolleyError error) {
                    if(maxTry==4){
                        Log.i("mytag", "try again connection timeout ");

                    }else{
                        maxTry++;
                        sendNewBandData();
                    }

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String,String> map=new HashMap<>();
                    map.put("makeNewBand",new Gson().toJson(newBandObject));
                    map.put("uPNumber",new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uPNumber));//get the number
//                    map.put("uPNumber",new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uPNumber));//get the number
                    Log.i("mytag", "makeNewBand "+new Gson().toJson(newBandObject));

                    return map;
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    8000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);


    }



}
