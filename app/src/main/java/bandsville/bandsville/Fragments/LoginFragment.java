package bandsville.bandsville.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandsville.bandsville.CustomSharedPref;
import bandsville.bandsville.DataStructure.Bands;
import bandsville.bandsville.DataStructure.Status;
import bandsville.bandsville.HomeActivity;
import bandsville.bandsville.Interface.ISharePreference;
import bandsville.bandsville.R;

public class LoginFragment extends Fragment {

    private EditText login_uPassword,login_uPNumber;
    private Button login_button;
    private int maxTry=0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mView=inflater.inflate(R.layout.login,container,false);
        login_uPNumber=mView.findViewById(R.id.login_uPNumber);
        login_uPassword=mView.findViewById(R.id.login_uPassword);
        login_button=mView.findViewById(R.id.login_button);
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!login_uPassword.equals("") && !login_uPNumber.equals("")) {

                    Login loginObject = new Login();
                    loginObject.setuPassword(login_uPassword.getText().toString().trim());
                    loginObject.setuPNumber(login_uPNumber.getText().toString().trim());

                    login(loginObject);

                }


            }
        });

        return mView;
    }


    private void login(final Login loginObject){
        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://192.168.1.111/Rachit/bandsville-php/authentication.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/authentication.php",
//                Value.SERVER_ADDRESS+"Payment.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag", "login response "+response);
                        Status status=new Gson().fromJson(response,Status.class);
                        maxTry=0;

                        if(status.getStatusCode().equals("success")){
                            Toast.makeText(getContext(),"Login Success",Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(getActivity(),HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }else{
                            Toast.makeText(getContext(),"Try again",Toast.LENGTH_SHORT).show();

                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if(maxTry==4){
                    Log.i("mytag", "try again connection timeout ");

                }else{
                    maxTry++;
                    login(loginObject);

                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("loginCredentials",new Gson().toJson(loginObject));
                Log.i("mytag", "login_credentials "+new Gson().toJson(loginObject));

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private class Login{
        private String uPNumber;
        private String uPassword;

        public String getuPNumber() {
            return uPNumber;
        }

        public void setuPNumber(String uPNumber) {
            this.uPNumber = uPNumber;
        }

        public String getuPassword() {
            return uPassword;
        }

        public void setuPassword(String uPassword) {
            this.uPassword = uPassword;
        }
    }


}



