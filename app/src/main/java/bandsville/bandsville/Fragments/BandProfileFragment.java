package bandsville.bandsville.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandsville.bandsville.CustomSharedPref;
import bandsville.bandsville.DataStructure.Members;
import bandsville.bandsville.DataStructure.Notification;
import bandsville.bandsville.DataStructure.Status;
import bandsville.bandsville.Interface.ISharePreference;
import bandsville.bandsville.R;

import static android.app.Activity.RESULT_OK;

public class BandProfileFragment extends Fragment{
    private TextView userName;
    private ImageView userPhoto;
    private ListView band_profile_fragment_lv;
    private List<Members> membersList=new ArrayList<>();
    private ArrayAdapter<Members> adapter;
    private int maxTry=0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View mView=inflater.inflate(R.layout.band_profile_fragment,container,false);
        band_profile_fragment_lv=mView.findViewById(R.id.band_profile_fragment_lv);
        adapter=new BandMemeberAdapter(getContext(),membersList);
        band_profile_fragment_lv.setAdapter(adapter);
        getBandInformation();
        return mView;
    }



    private class BandMemeberAdapter extends ArrayAdapter<Members>{

        public BandMemeberAdapter(@NonNull Context context, List<Members> membersList) {
            super(context, R.layout.band_fragment,membersList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            View view=convertView;
            if(convertView==null){
                LayoutInflater inflater=LayoutInflater.from(getContext());
                view=inflater.inflate(R.layout.band_profile_fragment_custom,parent,false);

            }

            TextView band_profile_fragment_custom_username=view.findViewById(R.id.band_profile_fragment_custom_username);
            TextView band_profile_fragment_custom_userType=view.findViewById(R.id.band_profile_fragment_custom_userType);
            TextView band_profile_fragment_custom_userScore=view.findViewById(R.id.band_profile_fragment_custom_userScore);

            band_profile_fragment_custom_username.setText(membersList.get(position).getmName());
            band_profile_fragment_custom_userType.setText(membersList.get(position).getmType());
            band_profile_fragment_custom_userScore.setText(membersList.get(position).getmScore());

            return view;
        }
    }


    private void getBandInformation(){
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                        "http://192.168.1.111/Rachit/bandsville-php/Notification.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/Notification.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag", "getBandInformation " + response);
                        Type listType = new TypeToken<List<Members>>() {}.getType();
                        membersList = new Gson().fromJson(response, listType);
                        //empty previous messages

                        adapter=new BandMemeberAdapter(getContext(),membersList);
                        band_profile_fragment_lv.setAdapter(adapter);

                        maxTry=0;//reset
                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if (maxTry == 4) {
                    Log.i("mytag", "try again connection timeout ");

                } else {
                    maxTry++;
                    getBandInformation();

                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<>();
                map.put("getBandsInformation", "yes");
                map.put("uPNumber", new CustomSharedPref(getContext()).getSharedPref(ISharePreference.uPNumber));

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }






}
