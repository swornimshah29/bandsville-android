package bandsville.bandsville.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandsville.bandsville.Authentication;
import bandsville.bandsville.CreateBand;
import bandsville.bandsville.CustomSharedPref;
import bandsville.bandsville.DataStructure.Bands;
import bandsville.bandsville.DataStructure.Status;
import bandsville.bandsville.HomeActivity;
import bandsville.bandsville.Interface.ISharePreference;
import bandsville.bandsville.R;

public class RegistrationFragment extends Fragment {

    private Button registration_type;
    private Button registration_btn;
    private EditText registration_uPNumber;
    private EditText registration_uName;
    private EditText registration_uPassword;
    private int maxTry=0;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mView=inflater.inflate(R.layout.registration,container,false);
        registration_uPNumber=mView.findViewById(R.id.registration_uPNumber);
        registration_type=mView.findViewById(R.id.registration_type);
        registration_uName=mView.findViewById(R.id.registration_uName);
        registration_uPassword=mView.findViewById(R.id.registration_uPassword);
        registration_btn=mView.findViewById(R.id.registration_btn);



        registration_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Registration registrationObject=new Registration();
                registrationObject.setuName(registration_uName.getText().toString().trim());
                registrationObject.setuPassword(registration_uPassword.getText().toString().trim());
                registrationObject.setuType(MusicianTypeFragment.mType);
                registrationObject.setuPNumber(registration_uPNumber.getText().toString().trim());
                registrationObject.setmLocationLat(new CustomSharedPref(getContext()).getSharedPref("USER_CURRENT_LOCATION_LAT"));
                registrationObject.setmLocationLon(new CustomSharedPref(getContext()).getSharedPref("USER_CURRENT_LOCATION_LON"));
                registrationObject.setuPNumber(registration_uPNumber.getText().toString().trim());

                if(!registrationObject.getuName().equals("") && registrationObject.getuPNumber().length()>=10 && !registrationObject.getuPassword().equals("")
                        && MusicianTypeFragment.mType!=null){
                    register(registrationObject);
                }else{
                    Toast.makeText(getContext(),"Enter valid inputs",Toast.LENGTH_SHORT).show();
                }

            }
        });

        registration_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //just show the gridview for types
                ((Authentication)getActivity()).getViewPager().setCurrentItem(2);

            }
        });

        return mView;
    }


    private void register(final Registration registrationObject){

        Log.i("mytag","localurl name "+new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/authentication.php");
        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
//                "http://192.168.1.111/Rachit/bandsville-php/authentication.php",
                new CustomSharedPref(getContext()).getSharedPref(ISharePreference.local_url)+"/authentication.php",
//                Value.SERVER_ADDRESS+"Payment.php",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("mytag", "registration response "+response);
                        Status status=new Gson().fromJson(response,Status.class);
                        if(status.getStatusCode().equals("success")){
                            new CustomSharedPref(getContext()).setSharedPref(ISharePreference.registrationSuccess,"success");
                            new CustomSharedPref(getContext()).setSharedPref(ISharePreference.uPNumber,registrationObject.getuPNumber());
                            new CustomSharedPref(getContext()).setSharedPref(ISharePreference.uName,registrationObject.getuName());
                            Toast.makeText(getContext(),"Sucessfully Registered",Toast.LENGTH_SHORT).show();
                            ((Authentication)getActivity()).getViewPager().setCurrentItem(1);//go to login

                            maxTry=0;
                        }else if(status.getStatusCode().equals("already")){
                            Toast.makeText(getContext(),"Already Registered",Toast.LENGTH_SHORT).show();
                            ((Authentication)getActivity()).getViewPager().setCurrentItem(1);//go to login
                            new CustomSharedPref(getContext()).setSharedPref(ISharePreference.registrationSuccess,"success");
                            new CustomSharedPref(getContext()).setSharedPref(ISharePreference.uPNumber,registrationObject.getuPNumber());
                            new CustomSharedPref(getContext()).setSharedPref(ISharePreference.uName,registrationObject.getuName());


                        }else{
                            Toast.makeText(getContext(),"Registration Failed Try again",Toast.LENGTH_SHORT).show();

                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if(maxTry==4){
                    Log.i("mytag", "try again connection timeout ");

                }else{
                    maxTry++;
                    register(registrationObject);

                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map=new HashMap<>();
                map.put("regCredentials",new Gson().toJson(registrationObject));
                Log.i("mytag", "regCredentials "+new Gson().toJson(registrationObject));

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES*2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private void musiciansType(){

    }


    //class for registration
    private class Registration{
        private String uPNumber;
        private String uName;
        private String uPassword;
        private String uType;
        private String mLocationLat;
        private String mLocationLon;

        public String getmLocationLat() {
            return mLocationLat;
        }

        public void setmLocationLat(String mLocationLat) {
            this.mLocationLat = mLocationLat;
        }

        public String getmLocationLon() {
            return mLocationLon;
        }

        public void setmLocationLon(String mLocationLon) {
            this.mLocationLon = mLocationLon;
        }

        public String getuPNumber() {
            return uPNumber;
        }

        public void setuPNumber(String uPNumber) {
            this.uPNumber = uPNumber;
        }

        public String getuName() {
            return uName;
        }

        public void setuName(String uName) {
            this.uName = uName;
        }

        public String getuPassword() {
            return uPassword;
        }

        public void setuPassword(String uPassword) {
            this.uPassword = uPassword;
        }

        public String getuType() {
            return uType;
        }

        public void setuType(String uType) {
            this.uType = uType;
        }
    }


    @Override
    public void onStart() {
        super.onStart();

    }
}
