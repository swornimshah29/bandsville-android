package bandsville.bandsville;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

import bandsville.bandsville.Fragments.CreateBandDetails;
import bandsville.bandsville.Fragments.CreateBandType;
import bandsville.bandsville.Fragments.LoginFragment;
import bandsville.bandsville.Fragments.RegistrationFragment;
import bandsville.bandsville.Interface.ISharePreference;

public class CreateBand extends AppCompatActivity {

    public ViewPager viewPager;
    private ViewPageAdapter viewPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_band);

        viewPager= findViewById(R.id.createBand_vp);
        viewPageAdapter=new ViewPageAdapter(getSupportFragmentManager());
        viewPageAdapter.addFragments(new CreateBandType());
        viewPageAdapter.addFragments(new CreateBandDetails());
        viewPager.setAdapter(viewPageAdapter);
        viewPager.setCurrentItem(0);

    }


    public class ViewPageAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> fragments = new ArrayList<>();

        public void addFragments(Fragment fragments) {
            this.fragments.add(fragments);
        }


        public ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {

            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Home";
                case 1:

                    return "Listen";
                case 2:

                    return "Notice";
                case 3:
                    return "Events";

            }
            return null;

        }


    }

    public ViewPager getViewPager() {
        if (null == viewPager) {
            viewPager = findViewById(R.id.createBand_vp);
        }
        return viewPager;
    }
}
