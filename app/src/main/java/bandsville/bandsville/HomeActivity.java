package bandsville.bandsville;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

import bandsville.bandsville.Fragments.BandFragment;
import bandsville.bandsville.Fragments.MusicianFragment;
import bandsville.bandsville.Fragments.NotificationFragment;
import bandsville.bandsville.Fragments.BandProfileFragment;

public class HomeActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPageAdapter viewPageAdapter;
    private FloatingActionButton createBandButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_activity);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPageAdapter = new ViewPageAdapter(getSupportFragmentManager());
        viewPageAdapter.addFragments(new MusicianFragment());
        viewPageAdapter.addFragments(new BandFragment());
        viewPageAdapter.addFragments(new BandProfileFragment());
        viewPageAdapter.addFragments(new NotificationFragment());

        viewPager.setAdapter(viewPageAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPageAdapter.setupTabIcons();


        createBandButton=findViewById(R.id.createBandButton);
        createBandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start the another activity with its own fragments
                startActivity(new Intent(HomeActivity.this,CreateBand.class));

            }
        });


    }



    public class ViewPageAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> fragments = new ArrayList<>();

        public void addFragments(Fragment fragments) {
            this.fragments.add(fragments);
        }

        public void setupTabIcons(){
            tabLayout.getTabAt(0).setIcon(R.mipmap.events_music);
            tabLayout.getTabAt(1).setIcon(R.mipmap.listen_songs);
            tabLayout.getTabAt(2).setIcon(R.mipmap.whitelove);
//            tabLayout.getTabAt(3).setIcon(R.mipmap.events_music);

        }


        public ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {

            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Musicians";
                case 1:

                    return "Bands";
                case 2:

                    return "Notification";

            }
            return null;

        }


    }
}
